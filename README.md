# nbn-ssg
A dead simple static site generator written in python.

---

### Dependencies

```pip install markdown2 dominate```

### Usage

* Clone the repo.

* Create a new project. `nbn-ssg.py --new [blog-name] --author [author-name]`

* Create a new post. `nbn-ssg.py --entry [post-title]`

* `cd` into the `posts` folder in the repo and edit the newly created markdown file with your content. Check the `posts.json` file to know the id for the newly created post file.


* Publish the post. `nbn-ssg.py --publish [post-id]|all`

* Generated output present in the `public` folder.

* Done! ^__^

* Optionally change the output path in the `conf.json` file.
