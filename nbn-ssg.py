# -*- coding: utf-8 -*-
import sys
import argparse
import json
import os.path
import time
from markdown2 import Markdown
import dominate
from dominate.tags import *
from dominate.util import raw
from distutils.dir_util import copy_tree

#constants
class CONST(object):
	#name of the config file
	CONF_FILE = "conf.json"
	#name of the file that stores the list of posts
	POSTS_FILE = "posts.json"
	#utils
	@staticmethod
	def read_conf():
		if(not os.path.isfile(CONST.CONF_FILE)):
			print("config.json not found. Aborting.")
			return False
		#if exists
		with open(CONST.CONF_FILE, 'r') as input:
			conf_data = json.load(input)
		return conf_data

	@staticmethod
	def get_post_index(posts_list, post_id):
		for i in range(len(posts_list)):
			if(int(posts_list[i]["id"]) == int(post_id)):
				return i
		return -1

	
#function to get all the arguments when the script is run.
def get_arguments():
	parser = argparse.ArgumentParser(description='A very simple static site generator.')
	parser.add_argument('-n', '--new', action='store', help='Create a new project.', nargs='?', dest='project_name')
	parser.add_argument('-a', '--author', action='store', help='Specify author for a new project', nargs='?', dest='author_name')
	parser.add_argument('-f', '--force', action='store_true', help='Forcibly create new project, overwrite previous '+CONST.CONF_FILE, dest='force_create')
	parser.add_argument('-e', '--entry', action='store', help='Generate a new entry(post).', nargs='?', dest='post_title')
	parser.add_argument('-p', '--publish', action='store', help='Build the final output of a post. [\'all\' for all posts]', nargs='?', dest='post_id')
	#parser.add_argument('-p', '--publish', action='store', help='Mark the post as publishable. (You need to run -b/--build to actually generate the output)', nargs='?', dest='publish_id')

	args = parser.parse_args()
	return args

def generate_post_by_id(post_id, post_index):
	try:
		post_id = int(post_id)
	except Exception as e:
		print('Invalid post id. Aborting.')
		return

	md = Markdown()
	post_title = None
	post_html = None

	#check for conf.json
	conf_data = CONST.read_conf()
	if(not conf_data):
		return

	#read list of all posts from posts.json
	with open(CONST.POSTS_FILE, 'r') as output:
		data = json.load(output) 
	
	current_id = data["current_id"]
	posts_list = data["posts"]

	#get post title start	------
	post_title = posts_list[post_index]["title"]
	#get post title end 	------

	#get post data start	------
	#read all the data from the markdown file of the given id.
	with open(conf_data["posts_path"]+'/'+str(post_id)+'.MD', 'r') as input:
		post_data = input.read()
	post_html = md.convert(post_data)
	#get post data end		------

	#read post header and post footer
	try:
		with open(conf_data["templates_path"]+"/posts/header.html.tpl", 'r') as input:
			post_header = input.read()

		with open(conf_data["templates_path"]+"/posts/footer.html.tpl", 'r') as input:
			post_footer = input.read()
	except Exception as e:
		try:
			with open(conf_data["templates_path"]+"/index/header.html.tpl", 'r') as input:
				post_header = input.read()

			with open(conf_data["templates_path"]+"/index/footer.html.tpl", 'r') as input:
				post_footer = input.read()
		except Exception as e2:
			print('Templates not found. Aborting.')
			return


	#check if ./pulbic/posts dir exists or not, create it if it doesn't
	public_posts_path = conf_data["output_path"]+"/posts/"
	if (not os.path.isdir(public_posts_path)):
		os.makedirs(public_posts_path)

	#build the final html for the post
	_html = html()
	_head = _html.add(head())
	_head.add(title(conf_data["name"]))
	_head.add(link(rel="stylesheet", type="text/css", href="../resources/css/index.css"))
	_head.add(link(rel="stylesheet", type="text/css", href="../resources/css/post.css"))

	_body = _html.add(body())
	_body.add(raw(post_header))

	_post_div = _body.add(div())
	
	_post_title_div = _post_div.add(div())

	_post_title_div.add(div(post_title, cls="post-title"))
	_post_title_div.add(div(posts_list[post_index]["created_at"], cls="created-at"))

	_post_div.add(raw(post_html))

	_body.add(raw(post_footer))

	with open(conf_data["output_path"]+'/posts/'+str(post_id)+'.html', 'w') as output:
		output.write(_html.render())

	#update the posts.json with created_at time
	if(posts_list[post_index]["created_at"] == -1):
		posts_list[post_index]["created_at"] = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
	else:
		posts_list[post_index]["modified_at"] = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

	data = {
		"current_id": current_id,
		"posts": posts_list
	}

	with open(CONST.POSTS_FILE, 'w') as output:
		json.dump(data, output, indent=4)

	print("Post with id "+str(post_id)+" generated.")
	return

def publish_post(post_id):
	#check for conf.json
	conf_data = CONST.read_conf()
	if(not conf_data):
		return

	#get the list of posts from posts.json
	with open(CONST.POSTS_FILE, 'r') as output:
		data = json.load(output) 
	
	current_id = data["current_id"]
	posts_list = data["posts"]

	#if post_id is not 'all', then publish the post with the given id
	if(post_id != "all"):
		post_index = CONST.get_post_index(posts_list, post_id)
		if(post_index != -1):
			if(posts_list[post_index]["publish"] == True):
				generate_post_by_id(post_id, post_index)
			else:
				print("Publish is set to false for the given post id. Aborting.")
				return
		else:
			print("Post not found is posts.json. Aborting.")
			return
	else:
		#iterate through all the id's in posts list and public all of them
		for i in range(len(posts_list)):
			if(posts_list[i]["publish"] == True):
				generate_post_by_id(posts_list[i]["id"], i)
	
	with open(CONST.POSTS_FILE, 'r') as output:
		data = json.load(output) 
	current_id = data["current_id"]
	posts_list = data["posts"]

	published_posts = []

	for i in range(len(posts_list)):
		if(posts_list[i]["created_at"] != -1 and posts_list[i]["publish"]):
			published_posts.append(posts_list[i])

	#read post header and post footer
	try:
		with open(conf_data["templates_path"]+"/index/header.html.tpl", 'r') as input:
			index_header = input.read()

		with open(conf_data["templates_path"]+"/index/footer.html.tpl", 'r') as input:
			index_footer = input.read()
	except Exception as e:
		print('Templates not found. Aborting.')
		return

	with open(CONST.POSTS_FILE, 'r') as output:
		data = json.load(output) 
	
	current_id = data["current_id"]
	posts_list = data["posts"]

	#build the index page
	_html = html()
	_head = _html.add(head())
	_head.add(title(conf_data["name"]))
	_head.add(link(rel="stylesheet", type="text/css", href="./resources/css/index.css"))

	_body = _html.add(body())
	_body.add(raw(index_header))
	_index_div = _body.add(div(cls="posts-list"))
	_body.add(raw(index_footer))

	#sort the posts by created_at
	posts_list.sort(key=lambda x: x["created_at"], reverse=True)

	#add the list of all posts to _index_div
	for i in range(len(posts_list)):
		if(posts_list[i]["publish"]):
			post_path = "./posts/"+str(posts_list[i]["id"])+".html"
			_post_div = _index_div.add(div())
			_post_title_div = _post_div.add(div(cls="post-entry"))
			_post_title_div.add(span(posts_list[i]["created_at"][:11], cls="timestamp"))
			_post_link = _post_title_div.add(a(posts_list[i]["title"],href=post_path))
			
	with open(conf_data["output_path"]+'/index.html', 'w') as output:
		output.write(_html.render())

	copy_tree('resources', conf_data["output_path"]+'/resources/')
	return		
			
def create_post(post_title):
	conf_data = CONST.read_conf()
	if(not conf_data):
		return

	with open(CONST.POSTS_FILE, 'r') as input:
		data = json.load(input)

	max_id = data["current_id"]
	next_id = max_id + 1

	new_post = {
		"id": next_id,
		"title" : post_title,
		"created_at": -1,
		"modified_at": -1,
		"publish": True
	}
	
	data["posts"].append(new_post)
	data["current_id"] = max_id + 1

	open(conf_data["posts_path"]+'/'+(str(next_id))+'.MD', 'w').close()

	with open(CONST.POSTS_FILE, 'w') as output:
		json.dump(data, output, indent=4)

	return

def create_project(project, author, force):
	details = {
		"posts_path": "posts",
		"templates_path": "templates",
		"output_path": "public"
	}
	posts = {
		"current_id": 0,
		"posts": []
	}
	#check for project name
	if(project != "" and project is not None):
		details["name"] = project
	else:
		print("Project name cannot be empty.")
		return
	
	#check for author name, if not present, use "Anon"
	if(author != "" and author is not None):
		details["author"] = author
	else:
		details["author"] = "Anon"
	
	#check if conf.json is already present
	if(os.path.isfile(CONST.CONF_FILE)):
		print(CONST.CONF_FILE+ " is already present.")
		if(force):
			print("Overwriting.")
		else:
			print("Aborting. Use -f or --force to overwrite existing "+CONST.CONF_FILE)
			return

	#check if posts.json is already present
	if(os.path.isfile(CONST.POSTS_FILE)):
		print(CONST.POSTS_FILE+ " is already present.")
		if(force):
			print("Overwriting.")
		else:
			print("Aborting. Use -f or --force to overwrite existing "+CONST.POSTS_FILE)
			return
	
	#create new conf.json is not already existing or if --force is used
	if(not os.path.isfile(CONST.CONF_FILE) or force):
		with open(CONST.CONF_FILE, 'w') as output:
			json.dump(details, output, indent=4)

	if(not os.path.isfile(CONST.POSTS_FILE) or force):
		with open(CONST.POSTS_FILE, 'w') as output:
			json.dump(posts, output, indent=4)


#main
def main():
	args = get_arguments()
	if(args.post_id is not None):
		publish_post(args.post_id)
	elif(args.post_title is not None):
		create_post(args.post_title)
	else:
		create_project(args.project_name, args.author_name, args.force_create)

		
if __name__ == "__main__":
	#execute main ^__^
	main()


